#!/bin/sh
apt update
sudo apt-get install git sudo make cmake -y

####################################################################################
#                                swap raspberry
####################################################################################
if [ "$(cat /proc/cpuinfo | grep 'Raspberry')" != "" ]; then
sudo systemctl stop dphys-swapfile.service
sudo systemctl disable dphys-swapfile.service
sudo update-rc.d dphys-swapfile remove
sudo chmod -x /etc/init.d/dphys-swapfile
sudo dphys-swapfile swapoff
sudo swapoff -a
sudo rm /var/swap
sudo dphys-swapfile uninstall
sudo sed -i 's/CONF_SWAPSIZE=.*/CONF_SWAPSIZE=0/' /etc/dphys-swapfile
fi
#
sudo systemctl stop rsyslog
sudo systemctl disable rsyslog
if [[ -f "/var/log/syslog*" ]]
then
	rm /var/log/syslog*
fi
if [[ -f "/var/log/*.log*" ]]
then
	rm /var/log/*.log*
fi


#########################################################################################################################
#                                     rtl-sdr
#########################################################################################################################
cat > /etc/modprobe.d/raspi-blacklist.conf <<- "EOF"
blacklist snd_bcm2835
# blacklist spi and i2c by default (many users don't need them)
#blacklist spi-bcm2708
#blacklist i2c-bcm2708
blacklist snd-soc-pcm512x
blacklist snd-soc-wm8804
# dont load default drivers for the RTL dongle
blacklist dvb_usb_rtl28xxu
blacklist rtl_2832
blacklist rtl_2830
EOF
################################

cd /opt
if [[ -d "/opt/rtl-sdr" ]]
then
	rm -r /opt/rtl-sdr
fi
if [[ -f "/etc/udev/rules.d/rtl-sdr.rules" ]]
then
 sudo rm /etc/udev/rules.d/rtl-sdr.rules
fi
if [[ -f "/etc/udev/rules.d/rtl-sdr.rules" ]]
then
 sudo rm /etc/udev/rules.d/rtl-sdr.rules
fi

sudo apt-get install cmake build-essential libusb-1.0-0-dev -y

wget --no-check-certificate 'https://drive.google.com/uc?export=download&id=14owBT_XWwA4uK5a4I8wepvd4YrC2JErh' -O '/opt/rtl-sdr-te1ws.zip'
sudo unzip rtl-sdr-te1ws.zip
sudo rm rtl-sdr-te1ws.zip

#git clone git://git.osmocom.org/rtl-sdr.git
cd rtl-sdr
mkdir build
cd build
cmake ../ -DINSTALL_UDEV_RULES=ON
make clean
make
make install
sudo ldconfig
sudo cp /opt/rtl-sdr/rtl-sdr.rules /etc/udev/rules.d



#########################################################################################################################
#                                                  Binary
#########################################################################################################################
if [[ -d "/opt/spyserver" ]]
then
	rm -r /opt/spyserver
fi
mkdir /opt/spyserver 
cd /opt/spyserver

if [ "$(uname -m | grep 'arm')" != "" ]; then
wget -O spyserver-arm32.tgz https://airspy.com/?ddownload=4247 -O /opt/spyserver/spyserver.tgz
elif [ "$(uname -m | grep 'aarch64')" != "" ]; then
wget -O spyserver-arm64.tgz https://airspy.com/?ddownload=5795 -O /opt/spyserver/spyserver.tgz
elif [ "$(uname -m | grep 'x86_64')" != "" ]; then
wget -O spyserver-linux-x64.tgz https://airspy.com/?ddownload=4262 -O /opt/spyserver/spyserver.tgz
elif [ "$(uname -m | grep 'i686')" != "" ]; then
wget -O spyserver-linux-x86.tgz https://airspy.com/?ddownload=4308 -O /opt/spyserver/spyserver.tgz
else
 echo "No Sistem Match"
fi
tar xvzf spyserver.tgz 
sudo chmod +x *


###############
sudo cat > /lib/systemd/system/airspy.service  <<- "EOF"
[Unit]
Description=AirSpy Service
Wants=network-online.target
#After=sound.target syslog.target
network-online.target

[Service]
User=root
Type=simple
Restart=always
RestartSec=3
StandardOutput=null
ExecStart=/opt/spyserver/spyserver  /opt/spyserver/spyserver.config

[Install]
WantedBy=multi-user.target
EOF
#####

sudo cat > /bin/menu-airspy <<- "EOF"
#!/bin/bash
while : ; do
choix=$(whiptail --title "AirSpy / Raspbian Proyect by HP3ICC 73." --menu "Suba o Baje con las flechas del teclado y seleccione el numero de opcion:" 16 65 7 \
1 " Config AirSpy Server" \
2 " Start & Restart AirSpy Server " \
3 " Stop AirSpy Server " \
4 " Salir del menu " 3>&1 1>&2 2>&3)
exitstatus=$?
#on recupere ce choix
#exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "Your chosen option:" $choix
else
    echo "You chose cancel."; break;
fi
# case : action en fonction du choix
case $choix in
1)
sudo nano /opt/spyserver/spyserver.config ;;
2)
sudo systemctl stop airspy.service && sudo systemctl start airspy.service && sudo systemctl enable airspy.service ;;
3)
sudo systemctl stop airspy.service && sudo systemctl disable airspy.service ;;
4)
break;
esac
done
exit 0

EOF
#
###
sudo systemctl daemon-reload
chmod +x  /bin/menu-airspy
clear
echo "###########################################################################################################################"
echo ""
echo "Install AirSpy Done"
echo ""
echo "type "menu-airspy" to config"
echo ""
echo "###########################################################################################################################"
sudo systemctl start airspy.service
sudo systemctl enable airspy.service
menu-airspy
