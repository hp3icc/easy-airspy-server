# Easy-AirSpy-Server

![alt text](https://gitlab.com/hp3icc/easy-airspy-server/-/raw/main/emq-airspy.png)

#

 ## Install terminal mode

into your ssh terminal copy and paste the following link :

    apt update

    apt-get install sudo curl -y
 
    sh -c "$(curl -fsSL https://gitlab.com/hp3icc/easy-airspy-server/-/raw/main/install.sh)"

    
#

 ## Dowload image Micro Sd to Raspberry X64 Pi3+ or P4


<p><a href="https://drive.google.com/u/0/uc?id=1GzgQ2GDzkCthZI_uFg_-DE3B-LME4tmx&export=download&confirm=t&uuid=4329f6b0-353d-44d1-b08b-2715faa32981" target="_blank">Click here to Dowload</a> image Raspberry X64 AirSpy Server&nbsp;</p>


 #

 ## Login console image Raspberry

 * user:

        pi

 * Password :

            Panama507

#

 ## Menu config 

to use the options menu, just type "menu-airspy" without the quotes in your ssh terminal or console.

#

## Patch Files

/opt/spyserver/

#

 ## Software AirSpy for Windows , with DMR decode pluggin

 
<p><a href="https://drive.google.com/u/0/uc?id=1UCiJ3RltLh19v6ZA9oThQoZw1VpenFJz&export=download&confirm=t&uuid=4329f6b0-353d-44d1-b08b-2715faa32981" target="_blank">Click here to Dowload</a> AirSpy X86 with DMR&nbsp;</p>


 #

 ## Source

 https://www.ea5wa.com/rtl-sdr/servidor-sdr-sobre-raspberry-pi

 https://airspy.com/
